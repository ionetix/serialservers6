//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Fri Jun 16 14:14:41 2017 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

`uselib lib=unisims_ver
`uselib lib=proc_common_v3_00_a

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  RX,                             // external RX pins
  TX,                             // external TX pins
  LEDS_CLK,                       // clock to LED driver
  LEDS_DATA,                      // data to LED driver
  LEDS_LATCH,                     // latch to LED driver
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_NUM_REG                      = 1;
parameter C_SLV_DWIDTH                   = 32;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input  [15:0]                             RX;
output [15:0]                             TX;
output                                    LEDS_CLK;
output                                    LEDS_DATA;
output                                    LEDS_LATCH;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [31 : 0]                       Bus2IP_Addr;
input                                     Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_REG-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_REG-1 : 0]              Bus2IP_WrCE;
output reg [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data = -1;
output reg                                IP2Bus_RdAck = 0;
output reg                                IP2Bus_WrAck = 0;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	reg [15:0] rx_sync = 16'hFFFF;            // synchronous version of RX input
  reg [15:0] rd = 16'h0;                    // RX FIFO read signal
  reg [15:0] preRd = 16'h0;                 // RX FIFO read signal to prevent duplicate reads from a single command
  reg [15:0] wr = 16'h0;                    // RX FIFO write signal
  reg [15:0] clkDivider[15:0];              // clock divider defaults to 115.2kbaud with a 50MHz clock
  reg [1:0] parityCfg[15:0];                // default to no parity
  reg [15:0] sevenBits = 0;                 // set to 1 for 7 data bits instead of 8
  reg [15:0] reset = 16'hFFFF;              // start in reset after power-up
  reg [15:0] okLed = 0;                     // OK LED status
  reg [15:0] failLed = 0;                   // Fail LED status
  wire [15:0] actyLed;                      // Activity LED status
  wire [7:0] rxData[15:0];                  // data in RX FIFO
  wire [15:0] rxFifoEmpty;                  // RX FIFO empty status
  wire [15:0] rxFifoFull;                   // RX FIFO full status
  wire [15:0] txFifoEmpty;                  // TX FIFO empty status
  wire [15:0] txFifoFull;                   // TX FIFO full status
  wire [15:0] t1;                           // Timeout bit if there is no activity for ~670ms
  wire [15:0] t2;                           // Timeout bit if there is no activity for ~5s

  initial
  begin
    clkDivider[0] = 434;
    clkDivider[1] = 434;
    clkDivider[2] = 434;
    clkDivider[3] = 434;
    clkDivider[4] = 434;
    clkDivider[5] = 434;
    clkDivider[6] = 434;
    clkDivider[7] = 434;
    clkDivider[8] = 434;
    clkDivider[9] = 434;
    clkDivider[10] = 434;
    clkDivider[11] = 434;
    clkDivider[12] = 434;
    clkDivider[13] = 434;
    clkDivider[14] = 434;
    clkDivider[15] = 434;
    parityCfg[0] = 0;
    parityCfg[1] = 0;
    parityCfg[2] = 0;
    parityCfg[3] = 0;
    parityCfg[4] = 0;
    parityCfg[5] = 0;
    parityCfg[6] = 0;
    parityCfg[7] = 0;
    parityCfg[8] = 0;
    parityCfg[9] = 0;
    parityCfg[10] = 0;
    parityCfg[11] = 0;
    parityCfg[12] = 0;
    parityCfg[13] = 0;
    parityCfg[14] = 0;
    parityCfg[15] = 0;
  end

	// AXI Bus memory map
	// 0: configuration register (r/w)
	// 1: status register (r/w - LED bits, read only - others)
	// 2: RX FIFO read register - read only (returns RX FIFO data and removes from FIFO)
	// 3: TX FIFO write register - write only (read returns RX FIFO data)
	assign IP2Bus_Error = 0;

	always @ (posedge Bus2IP_Clk)
	begin
		rx_sync <= RX;
    IP2Bus_WrAck <= Bus2IP_WrCE;
    IP2Bus_RdAck <= Bus2IP_RdCE;
    case (Bus2IP_Addr[3:2])
      2'h0: IP2Bus_Data <= {reset[Bus2IP_Addr[7:4]], 12'h0, sevenBits[Bus2IP_Addr[7:4]],
        parityCfg[Bus2IP_Addr[7:4]], clkDivider[Bus2IP_Addr[7:4]]};
      2'h1: IP2Bus_Data <= {24'h0, t2[Bus2IP_Addr[7:4]], t1[Bus2IP_Addr[7:4]],
        failLed[Bus2IP_Addr[7:4]], okLed[Bus2IP_Addr[7:4]],
        txFifoFull[Bus2IP_Addr[7:4]], txFifoEmpty[Bus2IP_Addr[7:4]],
        rxFifoFull[Bus2IP_Addr[7:4]], rxFifoEmpty[Bus2IP_Addr[7:4]]};
      default: IP2Bus_Data <= {24'h0, rxData[Bus2IP_Addr[7:4]]};
    endcase

    if (Bus2IP_WrCE)
    begin
      case (Bus2IP_Addr[7:2])
        6'h00: {reset[0], sevenBits[0], parityCfg[0], clkDivider[0]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h01: {failLed[0], okLed[0]} <= Bus2IP_Data[5:4];
        6'h04: {reset[1], sevenBits[1], parityCfg[1], clkDivider[1]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h05: {failLed[1], okLed[1]} <= Bus2IP_Data[5:4];
        6'h08: {reset[2], sevenBits[2], parityCfg[2], clkDivider[2]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h09: {failLed[2], okLed[2]} <= Bus2IP_Data[5:4];
        6'h0C: {reset[3], sevenBits[3], parityCfg[3], clkDivider[3]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h0D: {failLed[3], okLed[3]} <= Bus2IP_Data[5:4];
        6'h10: {reset[4], sevenBits[4], parityCfg[4], clkDivider[4]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h11: {failLed[4], okLed[4]} <= Bus2IP_Data[5:4];
        6'h14: {reset[5], sevenBits[5], parityCfg[5], clkDivider[5]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h15: {failLed[5], okLed[5]} <= Bus2IP_Data[5:4];
        6'h18: {reset[6], sevenBits[6], parityCfg[6], clkDivider[6]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h19: {failLed[6], okLed[6]} <= Bus2IP_Data[5:4];
        6'h1C: {reset[7], sevenBits[7], parityCfg[7], clkDivider[7]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h1D: {failLed[7], okLed[7]} <= Bus2IP_Data[5:4];
        6'h20: {reset[8], sevenBits[8], parityCfg[8], clkDivider[8]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h21: {failLed[8], okLed[8]} <= Bus2IP_Data[5:4];
        6'h24: {reset[9], sevenBits[9], parityCfg[9], clkDivider[9]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h25: {failLed[9], okLed[9]} <= Bus2IP_Data[5:4];
        6'h28: {reset[10], sevenBits[10], parityCfg[10], clkDivider[10]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h29: {failLed[10], okLed[10]} <= Bus2IP_Data[5:4];
        6'h2C: {reset[11], sevenBits[11], parityCfg[11], clkDivider[11]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h2D: {failLed[11], okLed[11]} <= Bus2IP_Data[5:4];
        6'h30: {reset[12], sevenBits[12], parityCfg[12], clkDivider[12]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h31: {failLed[12], okLed[12]} <= Bus2IP_Data[5:4];
        6'h34: {reset[13], sevenBits[13], parityCfg[13], clkDivider[13]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h35: {failLed[13], okLed[13]} <= Bus2IP_Data[5:4];
        6'h38: {reset[14], sevenBits[14], parityCfg[14], clkDivider[14]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h39: {failLed[14], okLed[14]} <= Bus2IP_Data[5:4];
        6'h3C: {reset[15], sevenBits[15], parityCfg[15], clkDivider[15]} <= {Bus2IP_Data[31], Bus2IP_Data[18:0]};
        6'h3D: {failLed[15], okLed[15]} <= Bus2IP_Data[5:4];
      endcase
    end
    wr[0] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h03);
    wr[1] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h07);
    wr[2] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h0B);
    wr[3] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h0F);
    wr[4] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h13);
    wr[5] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h17);
    wr[6] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h1B);
    wr[7] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h1F);
    wr[8] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h23);
    wr[9] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h27);
    wr[10] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h2B);
    wr[11] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h2F);
    wr[12] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h33);
    wr[13] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h37);
    wr[14] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h3B);
    wr[15] <= ~IP2Bus_WrAck & Bus2IP_WrCE & (Bus2IP_Addr[7:2] == 6'h3F);
    preRd[0] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h02};
    preRd[1] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h06};
    preRd[2] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h0A};
    preRd[3] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h0E};
    preRd[4] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h12};
    preRd[5] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h16};
    preRd[6] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h1A};
    preRd[7] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h1E};
    preRd[8] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h22};
    preRd[9] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h26};
    preRd[10] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h2A};
    preRd[11] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h2E};
    preRd[12] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h32};
    preRd[13] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h36};
    preRd[14] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h3A};
    preRd[15] <= Bus2IP_RdCE & {Bus2IP_Addr[7:2] == 6'h3E};
    rd[0] <= preRd[0] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h02));
    rd[1] <= preRd[1] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h06));
    rd[2] <= preRd[2] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h0A));
    rd[3] <= preRd[3] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h0E));
    rd[4] <= preRd[4] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h12));
    rd[5] <= preRd[5] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h16));
    rd[6] <= preRd[6] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h1A));
    rd[7] <= preRd[7] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h1E));
    rd[8] <= preRd[8] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h22));
    rd[9] <= preRd[9] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h26));
    rd[10] <= preRd[10] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h2A));
    rd[11] <= preRd[11] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h2E));
    rd[12] <= preRd[12] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h32));
    rd[13] <= preRd[13] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h36));
    rd[14] <= preRd[14] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h3A));
    rd[15] <= preRd[15] & (~Bus2IP_RdCE | ~(Bus2IP_Addr[7:2] == 6'h3E));
	end

  ledDriver ledDriver(.clk(Bus2IP_Clk), .flash(0), .acty(actyLed), .ok(okLed),
    .fail(failLed), .ledClk(LEDS_CLK), .ledLatch(LEDS_LATCH), .ledData(LEDS_DATA));

  genvar x;
  generate
    for (x=0; x < 16; x=x+1)
    begin
      rs232rx rs232rx(.clk(Bus2IP_Clk), .clkDiv(clkDivider[x]), .sevenBits(sevenBits[x]),
        .parity(parityCfg[x]), .reset(reset[x]), .rd(rd[x]), .rx(rx_sync[x]), .data(rxData[x]),
        .fifoEmpty(rxFifoEmpty[x]), .fifoFull(rxFifoFull[x]));
      rs232tx rs232tx(.clk(Bus2IP_Clk), .clkDiv(clkDivider[x]), .sevenBits(sevenBits[x]),
        .parity(parityCfg[x]), .reset(reset[x]), .data(Bus2IP_Data[7:0]), .wr(wr[x]), .tx(TX[x]),
        .fifoEmpty(txFifoEmpty[x]), .fifoFull(txFifoFull[x]));
      actyCtr actyCtr(.clk(Bus2IP_Clk), .rx(rx_sync[x]), .acty(actyLed[x]));
      timeout timeout(.clk(Bus2IP_Clk), .reset(~rx_sync[x] | ~TX[x]), .t1(t1[x]), .t2(t2[x]));
    end
  endgenerate
  
/*  reg [10:0] csDiv = 0;
  reg csClk = 1;
  always @ (posedge Bus2IP_Clk)
  begin
    csClk <= ~|csDiv;
    csDiv <= (csDiv >= 1301) ? 0 : csDiv + 1;
  end
  wire [35:0] csCtrl;
  wire [255:0] csData;
  chipscope_icon(.CONTROL0(csCtrl));
  chipscope_ila(.CONTROL(csCtrl), .CLK(Bus2IP_Clk), .TRIG0(csData));
  assign csData[15:0] = RX;
  assign csData[31:16] = TX;
  assign csData[47:32] = rx_sync;
  assign csData[63:48] = rd;
  assign csData[79:64] = wr;
  assign csData[95:80] = rxFifoEmpty;
  assign csData[111:96] = rxFifoFull;
  assign csData[127:112] = txFifoEmpty;
  assign csData[143:128] = txFifoFull;
  assign csData[159:144] = okLed;
  assign csData[175:160] = failLed;
  assign csData[191:176] = actyLed;
  assign csData[199:192] = rxData[2];
  assign csData[215:200] = clkDivider[2];
  assign csData[217:216] = parityCfg[2];
  assign csData[218] = sevenBits[2];
  assign csData[219] = reset[2];
  assign csData[235:220] = preRd;
  assign csData[243:236] = Bus2IP_Data[7:0];
  assign csData[249:244] = Bus2IP_Addr[5:0];
  assign csData[250] = t1[2];
  assign csData[251] = t2[2];
  assign csData[252] = Bus2IP_RdCE;
  assign csData[253] = Bus2IP_WrCE;
  assign csData[254] = Bus2IP_RNW;
  assign csData[255] = Bus2IP_CS;*/
endmodule
