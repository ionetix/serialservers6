`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    timeout
// Description: 	Timeout delay for serial transmissions
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module timeout(
	input clk,
  input reset,
  output reg t1 = 0,
  output reg t2 = 0
);

  reg [28:0] timer = 0;

	always @ (posedge clk)
	begin
    t1 <= |timer[28:25];
    t2 <= timer[28];
    if (reset)
      timer <= 0;
    else if (~timer[28])
      timer <= timer + 1;
	end
endmodule
