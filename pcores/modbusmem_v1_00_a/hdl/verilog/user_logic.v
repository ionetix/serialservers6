//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Fri Jun 16 14:14:41 2017 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

`uselib lib=unisims_ver
`uselib lib=proc_common_v3_00_a

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_NUM_REG                      = 1;
parameter C_SLV_DWIDTH                   = 32;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [31 : 0]                       Bus2IP_Addr;
input                                     Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_REG-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_REG-1 : 0]              Bus2IP_WrCE;
output     [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output reg                                IP2Bus_RdAck = 0;
output reg                                IP2Bus_WrAck = 0;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

  reg Bus2IP_WrCE_prev = 0;
  reg Bus2IP_RdCE_prev = 0;

	assign IP2Bus_Error = 0;

  always @ (posedge Bus2IP_Clk)
  begin
    Bus2IP_WrCE_prev <= Bus2IP_WrCE;
    IP2Bus_WrAck <= Bus2IP_WrCE;// & Bus2IP_WrCE_prev;
    Bus2IP_RdCE_prev <= Bus2IP_RdCE;
    IP2Bus_RdAck <= Bus2IP_RdCE;// & Bus2IP_RdCE_prev;
  end

//  reg web = 0;
//  reg [11:0] addrb = 0;
//  reg [31:0] dinb = 0;
//  wire [31:0] doutb;

  genvar x;
  generate
    for (x=0; x < 8; x=x+1) 
    begin: BRAMs
      RAMB16BWER #(.DATA_WIDTH_A(4), .DATA_WIDTH_B(4), .DOA_REG(0), .DOB_REG(0), .EN_RSTRAM_A("FALSE"), .EN_RSTRAM_B("FALSE"),
        .INITP_00(0), .INITP_01(0), .INITP_02(0), .INITP_03(0), .INITP_04(0), .INITP_05(0), .INITP_06(0), .INITP_07(0),
        .INIT_00(0), .INIT_01(0), .INIT_02(0), .INIT_03(0), .INIT_04(0), .INIT_05(0), .INIT_06(0), .INIT_07(0),
        .INIT_08(0), .INIT_09(0), .INIT_0A(0), .INIT_0B(0), .INIT_0C(0), .INIT_0D(0), .INIT_0E(0), .INIT_0F(0),
        .INIT_10(0), .INIT_11(0), .INIT_12(0), .INIT_13(0), .INIT_14(0), .INIT_15(0), .INIT_16(0), .INIT_17(0),
        .INIT_18(0), .INIT_19(0), .INIT_1A(0), .INIT_1B(0), .INIT_1C(0), .INIT_1D(0), .INIT_1E(0), .INIT_1F(0),
        .INIT_20(0), .INIT_21(0), .INIT_22(0), .INIT_23(0), .INIT_24(0), .INIT_25(0), .INIT_26(0), .INIT_27(0),
        .INIT_28(0), .INIT_29(0), .INIT_2A(0), .INIT_2B(0), .INIT_2C(0), .INIT_2D(0), .INIT_2E(0), .INIT_2F(0),
        .INIT_30(0), .INIT_31(0), .INIT_32(0), .INIT_33(0), .INIT_34(0), .INIT_35(0), .INIT_36(0), .INIT_37(0),
        .INIT_38(0), .INIT_39(0), .INIT_3A(0), .INIT_3B(0), .INIT_3C(0), .INIT_3D(0), .INIT_3E(0), .INIT_3F(0),
        .INIT_A(0), .INIT_B(0), .INIT_FILE("NONE"), .RSTTYPE("SYNC"), .RST_PRIORITY_A("CE"), .RST_PRIORITY_B("CE"),
        .SIM_COLLISION_CHECK("ALL"), .SIM_DEVICE("SPARTAN6"), .SRVAL_A(36'h000000000), .SRVAL_B(36'h000000000),
        .WRITE_MODE_A("WRITE_FIRST"), .WRITE_MODE_B("WRITE_FIRST")) modbusmem_inst (
        .DOA(IP2Bus_Data[4*x+3:4*x]), .DOPA(), .DOB(), .DOPB(),
        .ADDRA(Bus2IP_Addr[13:2]), .CLKA(Bus2IP_Clk), .ENA(1'b1), .REGCEA(1'b1), .RSTA(1'b0), .WEA(Bus2IP_WrCE),
        .DIA(Bus2IP_Data[4*x+3:4*x]), .DIPA(4'h0), 
        .ADDRB(0), .CLKB(Bus2IP_Clk), .ENB(1'b0), .REGCEB(1'b1), .RSTB(1'b0), .WEB(0),
        .DIB(0), .DIPB(4'h0));
    end
  endgenerate

/*  reg web = 0;
  reg [11:0] addrb = 0;
  reg [31:0] dinb = 0;
  wire [31:0] doutb;
  mem32 mem0 (.clka(Bus2IP_Clk), .wea(Bus2IP_WrCE),
    .addra(Bus2IP_Addr[13:2]), .dina(Bus2IP_Data), .douta(IP2Bus_Data),
    .clkb(Bus2IP_Clk), .web(web), .addrb(addrb), .dinb(dinb), .doutb(doutb));
    
  always @ (posedge Bus2IP_Clk)
  begin
    dinb <= dinb + 1;
    addrb <= ~dinb[31:20];
    if (&dinb)
      web <= ~web;
  end*/
    
  wire [35:0] csCtrl;
  wire [255:0] csData;
  chipscope_icon(.CONTROL0(csCtrl));
  chipscope_ila(.CONTROL(csCtrl), .CLK(Bus2IP_Clk), .TRIG0(csData));
  assign csData[31:0] = Bus2IP_Addr;
  assign csData[63:32] = Bus2IP_Data;
  assign csData[95:64] = IP2Bus_Data;
  assign csData[99:96] = Bus2IP_BE;
  assign csData[100] = Bus2IP_Resetn;
  assign csData[101] = Bus2IP_CS;
  assign csData[102] = Bus2IP_RNW;
  assign csData[103] = Bus2IP_RdCE;
  assign csData[104] = Bus2IP_WrCE;
  assign csData[105] = IP2Bus_RdAck;
  assign csData[106] = IP2Bus_WrAck;
  assign csData[107] = IP2Bus_Error;
  assign csData[255:108] = 0;
//  assign csData[179] = web;
//  assign csData[191:180] = addrb;
//  assign csData[223:192] = doutb;
//  assign csData[255:224] = dinb;
endmodule
