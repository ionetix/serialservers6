//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Tue Nov 25 00:08:28 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module user_logic
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  // --USER ports added here 
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
  Bus2IP_Clk,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  Bus2IP_Addr,                    // Bus to IP address bus
  Bus2IP_CS,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  Bus2IP_Data,                    // Bus to IP data bus
  Bus2IP_BE,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  IP2Bus_Data,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
parameter C_SLV_AWIDTH                   = 32;
parameter C_SLV_DWIDTH                   = 32;
parameter C_NUM_MEM                      = 1;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
// --USER ports added here 
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
input                                     Bus2IP_Clk;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           Bus2IP_Addr;
input      [C_NUM_MEM-1 : 0]              Bus2IP_CS;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           Bus2IP_Data;
input      [C_SLV_DWIDTH/8-1 : 0]         Bus2IP_BE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output reg [C_SLV_DWIDTH-1 : 0]           IP2Bus_Data;
output reg                                IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	reg [15:0] crcIn = 16'hFFFF;
	reg busyIn = 0;
	reg [2:0] ctrIn = 0;
	reg inWrAck = 0;
	reg [15:0] crcOut = 16'hFFFF;
	reg busyOut = 0;
	reg [2:0] ctrOut = 0;
	reg outWrAck = 0;
	reg [31:0] timer = 0;
	reg [7:0] slaveAddr = 1;
  reg loadSettings = 0;
  reg saveSettings = 0;

	assign IP2Bus_Error = 0;
	assign IP2Bus_AddrAck = IP2Bus_RdAck | IP2Bus_WrAck;
	assign IP2Bus_WrAck = Bus2IP_CS[0] & ~Bus2IP_RNW;

	// Address 0: write of any value resets incoming packet CRC calculation
	// Address 1: write new value to update incoming packet CRC
	// Address 2: write of any value resets outgoing packet CRC calculation
	// Address 3: write new value to update outgoing packet CRC
	// Address 0,1: read - get incoming packet CRC
	// Address 2,3: read - get outgoing packet CRC
	// Address 4: write of any value clears timer
	// Address 4: read - get current timer value
	// Address 5: read/write slave address
  // Address 6: load settings from flash (flag for software)
  // Address 7: save settings to flash (flag for software)
	always @*
	begin
		case (Bus2IP_Addr[4:2])
			3'h0:	IP2Bus_Data <= {16'h0, crcIn};
			3'h1: IP2Bus_Data <= {16'h0, crcIn};
			3'h2: IP2Bus_Data <= {16'h0, crcOut};
			3'h3: IP2Bus_Data <= {16'h0, crcOut};
			3'h4: IP2Bus_Data <= timer;
			3'h5: IP2Bus_Data <= {24'h0, slaveAddr};
      3'h6: IP2Bus_Data <= loadSettings;
      3'h7: IP2Bus_Data <= saveSettings;
		endcase
	end

	always @ (posedge Bus2IP_Clk)
	begin
		// initialize incoming packet CRC
		if (Bus2IP_CS & ~Bus2IP_RNW & |Bus2IP_BE & (Bus2IP_Addr[4:2] == 3'h0))
		begin
			crcIn <= 16'hFFFF;
			busyIn <= 0;
			inWrAck <= 1;
			ctrIn <= 0;
		end
		// update incoming packet CRC with data byte
		else
		begin
			if (Bus2IP_CS & ~Bus2IP_RNW & &Bus2IP_BE[1:0] & (Bus2IP_Addr[4:2] == 3'h1) & ~busyIn)
			begin
				crcIn <= crcIn ^ {8'h00, Bus2IP_Data[7:0]};
				busyIn <= 1;
				inWrAck <= 1;
				ctrIn <= 0;
			end
			else
			begin
				if (busyIn)
				begin
					crcIn <= crcIn[0] ? (crcIn >> 1) ^ 16'hA001 : crcIn >> 1;
					ctrIn <= ctrIn + 1;
					if (&ctrIn)
						busyIn <= 0;
				end
				inWrAck <= 0;
			end
		end
		// initialize outgoing packet CRC
		if (Bus2IP_CS & ~Bus2IP_RNW & |Bus2IP_BE & (Bus2IP_Addr[4:2] == 3'h2))
		begin
			crcOut <= 16'hFFFF;
			busyOut <= 0;
			outWrAck <= 1;
			ctrOut <= 0;
		end
		// update incoming packet CRC with data byte
		else
		begin
			if (Bus2IP_CS & ~Bus2IP_RNW & &Bus2IP_BE[1:0] & (Bus2IP_Addr[4:2] == 3'h3) & ~busyOut)
			begin
				crcOut <= crcOut ^ {8'h00, Bus2IP_Data[7:0]};
				busyOut <= 1;
				outWrAck <= 1;
				ctrOut <= 0;
			end
			else
			begin
				if (busyOut)
				begin
					crcOut <= crcOut[0] ? (crcOut >> 1) ^ 16'hA001 : crcOut >> 1;
					ctrOut <= ctrOut + 1;
					if (&ctrOut)
						busyOut <= 0;
				end
				outWrAck <= 0;
			end
		end
		
		// On a write, reset the counter
		if (Bus2IP_CS & ~Bus2IP_RNW & |Bus2IP_BE & (Bus2IP_Addr[4:2] == 3'h4))
			timer <= 0;
		else if (~&timer)
			timer <= timer + 1;
			
		// Set the Modbus slave address (no change on invalid address)
		if (Bus2IP_CS & ~Bus2IP_RNW & (Bus2IP_Addr[4:2] == 3'h5) & |Bus2IP_Data[7:0] & (Bus2IP_Data[7:0] < 248))
			slaveAddr <= Bus2IP_Data[7:0];

		// On CRC read, wait until any update is finished
		if (((Bus2IP_Addr[4:3] == 2'b00) & busyIn) | ((Bus2IP_Addr[4:3] == 2'b01) & busyOut))
			IP2Bus_RdAck <= 0;
		else
			IP2Bus_RdAck <= Bus2IP_CS & Bus2IP_RNW;
    // write to load settings flag
    if (Bus2IP_CS & ~Bus2IP_RNW & Bus2IP_BE[0] & (Bus2IP_Addr[4:2] == 3'h6))
      loadSettings <= Bus2IP_Data[0];
    // write to save settings flag
    if (Bus2IP_CS & ~Bus2IP_RNW & Bus2IP_BE[0] & (Bus2IP_Addr[4:2] == 3'h7))
      saveSettings <= Bus2IP_Data[0];
	end
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon(.CONTROL0(csCtrl));
	chipscope_ila csIla(.CONTROL(csCtrl), .CLK(Bus2IP_Clk), .TRIG0(csData));
	assign csData[31:0] = Bus2IP_Addr;
	assign csData[63:32] = Bus2IP_Data;
	assign csData[95:64] = IP2Bus_Data;
	assign csData[99:96] = Bus2IP_BE[1:0];
	assign csData[100] = Bus2IP_Resetn;
	assign csData[101] = Bus2IP_CS;
	assign csData[102] = Bus2IP_RNW;
	assign csData[103] = Bus2IP_RdCE;
	assign csData[104] = Bus2IP_WrCE;
	assign csData[105] = IP2Bus_RdAck;
	assign csData[106] = IP2Bus_WrAck;
	assign csData[107] = IP2Bus_Error;
	assign csData[123:108] = 0;//crc;
	assign csData[126:124] = 0;//ctr;
	assign csData[127] = 0;//busy;
	assign csData[128] = IP2Bus_AddrAck;
	assign csData[160:129] = 0;
	assign csData[161] = 0;
	assign csData[255:162] = 0;*/

endmodule
