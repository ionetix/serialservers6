`timescale 1ns / 1ps
module tempSensor(
	input clk,
	output reg temp_clk = 0,
	output reg temp_cs = 0,
	input temp_data,
	output reg [31:0] temp_inst = 0,
	output reg [31:0] temp_avg = 32'hFFFFFFFF
	);

  reg [1:0] div = 0;
	reg [18:0] ctr = 0;
	reg [15:0] shifter = 0;
	reg [15:0] tempRaw;
	wire [31:0] tempMult;
	always @ (posedge clk)
	begin
    div <= div + 1;
    if (&div)
    begin
      ctr <= ctr + 1;
      temp_clk <= ctr[0] & (ctr < 44);
      temp_cs <= (ctr > 43);
      if (ctr[0])
        shifter <= {shifter[15:0], temp_data};
      if (ctr == 44)
        tempRaw <= shifter;
      temp_inst <= tempMult - 32'h00320000;		// temp (degrees C) = 330 * RAW - 50
      if (&ctr)
        temp_avg <= &temp_avg ? temp_inst : temp_avg + (temp_inst >> 8) - (temp_avg >> 8);
    end
	end
	tempSensorMult multBy330 (.clk(clk), .a(tempRaw), .p(tempMult));
endmodule
